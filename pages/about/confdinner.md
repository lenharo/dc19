---
name: Conference Dinner
---
# Conference Dinner

Every year, we celebrate DebConf with a special dinner.

## When?

 * July 25th, 2019 (Thursday).
 * 6 PM.

## Where?

To be defined.

## Cost

If you have a food bursary, the conference dinner is included.

If you are self-paying for food, the conference dinner will cost the same as a
regular DebConf dinner, make sure to purchase dinner for the Thursday night
during registration, or from the front desk.

## Food

To be defined.

## Drinks

To be defined.

### Beer

To be defined.

### Wine

To be defined.

### Non-alcoholic

To be defined.
