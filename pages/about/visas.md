---
name: Visas
---
# Visas

Brazil operates its borders on a reciprocity policy, so if your country requires
a visa from Brazilian citizens, most likely Brazil will require a visa from you.

* South American citizens do not need a visa;
* European citizens from countries in the Schengen area do not need a visa;
* US/Canadian citizens do need a visa;
* Other countries most likely require a visa.

Please check the [official listing of visa requirements for entering Brazil][visa-reqs].
Wikipedia also has [a page on the subject][wp-visas], which you may find
useful.

[visa-reqs]: http://www.portalconsular.itamaraty.gov.br/images/qgrv/QGRVsimples.ing.11.12.pdf
[wp-visas]: https://en.wikipedia.org/wiki/Visa_policy_of_Brazil

If you need any help obtaining a visa, please email the visa team
<[visa@debconf.org][]> with your details.

[visa@debconf.org]: mailto:visa@debconf.org

Please bring a printout of your registration confirmation email, for
immigration officers to examine.
